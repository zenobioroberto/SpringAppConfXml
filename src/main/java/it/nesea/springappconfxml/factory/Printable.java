package it.nesea.springappconfxml.factory;

public interface Printable {
    void print();
}
