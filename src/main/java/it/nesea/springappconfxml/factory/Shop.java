package it.nesea.springappconfxml.factory;

public class Shop implements Printable{
    private static Shop instance = null;
    private Shop(){}

    public static Shop getInstance(){
        if(instance==null)
            synchronized(Shop.class) {
                if( instance == null )
                    instance = new Shop();
            }
        return instance;
    }

    public void print() {
        System.out.println("Hello from Shop View!");
    }
}
