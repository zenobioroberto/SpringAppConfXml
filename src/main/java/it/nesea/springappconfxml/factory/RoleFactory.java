package it.nesea.springappconfxml.factory;

import it.nesea.springappconfxml.beans.Role;
import it.nesea.springappconfxml.beans.User;

import java.util.Arrays;

public class RoleFactory implements Printable{
    public static User getAdmin(){
        Role adminRole = new Role("ADMIN", "ADMIN");
        return new User(1, "admin", "admin", Arrays.asList(adminRole));
    }

    public void print() {
        System.out.println("Hello from RoleFactory!");
    }
}
