package it.nesea.springappconfxml.beans;

import java.util.LinkedList;
import java.util.List;

public class User {
    private Integer id;
    private String name;
    private String lastName;
    private List<Role> roles = new LinkedList<Role>();

    public User() {
    }

    public User(Integer id, String name, String lastName) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
    }

    public User(Integer id, String name, String lastName, List<Role> roles) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.roles = roles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", roles=" + roles +
                '}';
    }
}
