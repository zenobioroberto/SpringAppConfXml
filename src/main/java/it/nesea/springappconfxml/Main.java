package it.nesea.springappconfxml;

import it.nesea.springappconfxml.beans.Role;
import it.nesea.springappconfxml.beans.User;
import it.nesea.springappconfxml.factory.Printable;
import it.nesea.springappconfxml.factory.Shop;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main (String args []){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
        printBeanDefInContext(ctx);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
        gamingWithAutowiring(ctx);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
        beanContextTest(ctx);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
    }

    /**
     * In questo metodo andremo a fare un pò di prove con l'autowire e le varie tipoogie
     * possibili (no, byType, byName e constructor) osservando come i risultati sono diversi
     * pur partendo dallo stesso container di base.
     * @param ctx
     */
    private static void gamingWithAutowiring(ApplicationContext ctx) {
        User userAConstructor = (User)ctx.getBean("userAConstructor");
        System.out.println("userAConstructor = " + userAConstructor);
        User userAbyType = (User)ctx.getBean("userAbyType");
        System.out.println("userAbyType = " + userAbyType);
        User userAbyName = (User)ctx.getBean("userAbyName");
        System.out.println("userAbyName = " + userAbyName);
        User userANo = (User)ctx.getBean("userANo");
        System.out.println("userANo = " + userANo);
        User userA = (User)ctx.getBean("userA");
        System.out.println("userA = " + userA);
        Role roleAutowire = (Role)ctx.getBean("roleAutowire");
        System.out.println("roleAutowire = " + roleAutowire);
    }

    /**
     * Con questo metodo possiamo osservare costantemente, quanto succede all'interno dell'ApplicationContext
     * In particolare, abbiamo modo di stampare un elenco di tutti i bean presenti
     * @param ctx
     */
    private static void printBeanDefInContext(ApplicationContext ctx) {
        for (String name : ctx.getBeanDefinitionNames()){
             System.out.println("name = " + name);
         }
    }

    /**
     * Usiamo i bean definiti all'interno di beans.xml
     * In particolare vediamo come questi possono essere costruiti
     * inizializzati e configurati con le tecniche presenti sulle slide
     * @param ctx
     */
    private static void beanContextTest(ApplicationContext ctx) {
        User user = (User)ctx.getBean("user");
        User moderator = (User)ctx.getBean("moderator");
        Role role = (Role)ctx.getBean("role");
        System.out.println("user = " + user);
        System.out.println("user = " + moderator);
        System.out.println("role = " + role);

        Shop shop = (Shop)ctx.getBean("shop");
        System.out.println("shop = " + shop);

        User admin = (User)ctx.getBean("adminUser");
        System.out.println("admin = " + admin);

        Printable printable = (Printable)ctx.getBean("printable");
        printable.print();

        printable =(Printable)ctx.getBean("printableNS");
        printable.print();
    }
}
